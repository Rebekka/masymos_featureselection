package de.unirostock.query;

public interface IResultSetInterface {
	
	public float getScore();
	
	public Long getDatabaseID();
	
	public String getSearchExplanation();
}
