package de.unirostock.extractor;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;

import de.unirostock.configuration.NodeLabel;
import de.unirostock.configuration.Property;
import de.unirostock.configuration.Relation;
import de.unirostock.configuration.Relation.DatabaseRelTypes;
import de.unirostock.configuration.Relation.DocumentRelTypes;
import de.unirostock.data.PersonWrapper;
import de.unirostock.data.PublicationWrapper;
import de.unirostock.database.Manager;
import de.unirostock.extractor.CellML.CellMLExtractor;
import de.unirostock.extractor.SBML.SBMLExtractor;
import de.unirostock.extractor.SedML.SEDMLExtractor;
import de.unirostock.extractor.XML.XMLExtractor;
import de.unirostock.query.enumerator.PersonFieldEnumerator;
import de.unirostock.query.enumerator.PublicationFieldEnumerator;
import de.unirostock.query.types.PersonQuery;
import de.unirostock.query.types.PublicationQuery;

public abstract class Extractor {
	protected static Index<Node> publicationIndex = Manager.instance().getPublicationIndex();
	protected static Index<Node> personIndex = Manager.instance().getPersonIndex();
	protected static Index<Node> modelIndex = Manager.instance().getModelIndex();	
	protected static Index<Relationship> relationshipIndex = Manager.instance().getRelationshipIndex();
	protected static Index<Node> annotationIndex = Manager.instance().getAnnotationIndex();
	protected static Index<Node> constitientIndex = Manager.instance().getConstituentIndex();
	protected static GraphDatabaseService graphDB = Manager.instance().getDatabase();
	protected static Index<Node> sedmlIndex = Manager.instance().getSedmlIndex();
	
	public static Node setExternalDocumentInformation(Node documentNode, Map<String, String> propertyMap){
		if (propertyMap==null) return documentNode;
		
		for (Iterator<String> iterator = propertyMap.keySet().iterator(); iterator.hasNext();) {
			String key = (String) iterator.next();
			Transaction tx = graphDB.beginTx();
			try {
				documentNode.setProperty(key, propertyMap.get(key));
				tx.success();
			} catch (IllegalArgumentException e) {
				tx.failure();
			} finally {
				tx.finish();
			}
			
		}
		return documentNode;
	}
	
	public static Node setExternalDocumentInformation(Node documentNode, String filepath, String documentURI ){
		if (StringUtils.isEmpty(documentURI)) documentURI = "";
		if (StringUtils.isEmpty(filepath)) filepath = "";
			Transaction tx = graphDB.beginTx();
			try {
				documentNode.setProperty(Property.General.FILENAME, filepath);
				documentNode.setProperty(Property.General.URI, documentURI);
				tx.success();
			} catch (IllegalArgumentException e) {
				tx.failure();
			} finally {
				tx.finish();
			}
			

		return documentNode;
	}
	
	public static Node extractStoreIndex(String path, String modelType)
			throws XMLStreamException, IOException {
		switch(modelType){
		case 	Property.ModelType.SBML : return SBMLExtractor.extractStoreIndex(new FileInputStream(path), null);									 
		case 	Property.ModelType.CELLML : return CellMLExtractor.extractStoreIndex(path, null);
		case 	Property.ModelType.SEDML : return SEDMLExtractor.extractStoreIndex(new File(path), null);
		default : return XMLExtractor.extractStoreIndex(new FileInputStream(path), null);
		}		

	}

	public static Node extractStoreIndex(byte[] byteArray, String modelType)
			throws XMLStreamException, IOException {
		switch(modelType){
		case 	Property.ModelType.SBML : return SBMLExtractor.extractStoreIndex(new ByteArrayInputStream(byteArray), null);									 
		case 	Property.ModelType.CELLML : {
			File temp = File.createTempFile("graphstore_model_temp", "xml");
			FileUtils.writeByteArrayToFile(temp, byteArray);			
			Node n = CellMLExtractor.extractStoreIndex(temp.getPath(), null);
			temp.delete();
			return n;
		}
		case 	Property.ModelType.SEDML : {
			File temp = File.createTempFile("graphstore_sed_temp", "xml");
			FileUtils.writeByteArrayToFile(temp, byteArray);			
			Node n = SEDMLExtractor.extractStoreIndex(temp, null, null);
			temp.delete();
			return n;
		}
		default : return XMLExtractor.extractStoreIndex(new ByteArrayInputStream(byteArray), null);
		}	
		
	}
	
	public static Node extractStoreIndex(URL url, String modelType, String versionId)
			throws XMLStreamException, IOException {
		switch(modelType){
		case 	Property.ModelType.SBML : return SBMLExtractor.extractStoreIndex(url.openStream(), versionId);									 
		case 	Property.ModelType.CELLML : return CellMLExtractor.extractStoreIndex(url.toString(), versionId);
		case 	Property.ModelType.SEDML : {		
					File temp = File.createTempFile(url.getFile(), "xml");
					FileUtils.copyURLToFile(url, temp);			
					Node n = SEDMLExtractor.extractStoreIndex(temp, null, null);
					temp.delete();
					return n;
			} 
		default : return XMLExtractor.extractStoreIndex(url.getFile(), versionId);
		}	
		
	}
	
	public static Node extractStoreIndex(File file, String modelType)
			throws XMLStreamException, IOException {
		switch(modelType){
		case 	Property.ModelType.SBML : return SBMLExtractor.extractStoreIndex(new FileInputStream(file),  null);									 
		case 	Property.ModelType.CELLML : return CellMLExtractor.extractStoreIndex(file.getPath(),  null);
		case 	Property.ModelType.SEDML : return SEDMLExtractor.extractStoreIndex(file,  null);
		default : return XMLExtractor.extractStoreIndex(new FileInputStream(file),  null);
		}			
	}
	
	public static Node extractStoreIndex(String path, PublicationWrapper publication, String versionID, String modelType)
			throws XMLStreamException, IOException {
		switch(modelType){
		case 	Property.ModelType.SBML : return SBMLExtractor.extractStoreIndex(new FileInputStream(path),  versionID);									 
		case 	Property.ModelType.CELLML : return CellMLExtractor.extractStoreIndex(path, versionID);
		case 	Property.ModelType.SEDML : return SEDMLExtractor.extractStoreIndex(new File(path), versionID);
		default : return XMLExtractor.extractStoreIndex(new FileInputStream(path), versionID);
		}		
	}

	public static Node extractStoreIndex(byte[] byteArray, PublicationWrapper publication, String versionID, String modelType)
			throws XMLStreamException, IOException {
		switch(modelType){
		case 	Property.ModelType.SBML : return SBMLExtractor.extractStoreIndex(new ByteArrayInputStream(byteArray), versionID);										 
		case 	Property.ModelType.CELLML : {
			
			File temp = File.createTempFile("graphstore_model_temp", "xml");
			FileUtils.writeByteArrayToFile(temp, byteArray);			
			Node n = CellMLExtractor.extractStoreIndex(temp.getPath(), versionID);
			temp.delete();
			return n;
		}
		case 	Property.ModelType.SEDML : {
			
			File temp = File.createTempFile("graphstore_model_temp", "xml");
			FileUtils.writeByteArrayToFile(temp, byteArray);			
			Node n = SEDMLExtractor.extractStoreIndex(temp, publication, versionID);
			temp.delete();
			return n;
		}
		default : return XMLExtractor.extractStoreIndex(new ByteArrayInputStream(byteArray), versionID);
		}
	}
	
	public static Node extractStoreIndex(File file, PublicationWrapper publication, String versionID, String modelType)
			throws XMLStreamException, IOException {		
		switch(modelType){
		case 	Property.ModelType.SBML : return SBMLExtractor.extractStoreIndex(new FileInputStream(file), versionID);										 
		case 	Property.ModelType.CELLML : return CellMLExtractor.extractStoreIndex(file.getPath(),  versionID);
		case 	Property.ModelType.SEDML : return SEDMLExtractor.extractStoreIndex(file, versionID);
		default : return XMLExtractor.extractStoreIndex(new FileInputStream(file),versionID);
		}
		
	}
	
	protected static void processPublication(PublicationWrapper publication, Node referenceNode, Node modelNode) {
		//all Strings null -> ""
		publication.repairNullStrings();
		
		Node publicationNode = null;
		try {
			PublicationQuery pq = new PublicationQuery();
			pq.addQueryClause(PublicationFieldEnumerator.PUBID, publication.getPubid());			
			publicationNode = publicationIndex.query(pq.getQuery()).getSingle();
		} catch (NoSuchElementException  e) {
			publicationNode = null;
		}
		if (publicationNode==null) {
			publicationNode = graphDB.createNode();	
			
			publicationNode.setProperty(Property.Publication.ID, publication.getPubid());
			publicationIndex.add(publicationNode,Property.Publication.ID, publication.getPubid());
			publicationNode.setProperty(Property.Publication.ABSTRACT, publication.getSynopsis());
			publicationIndex.add(publicationNode,Property.Publication.ABSTRACT, publication.getSynopsis());
			publicationNode.setProperty(Property.Publication.ABSTRACT, publication.getSynopsis());
			publicationIndex.add(publicationNode,Property.Publication.ABSTRACT, publication.getSynopsis());
			publicationNode.setProperty(Property.Publication.AFFILIATION, publication.getAffiliation());
			publicationIndex.add(publicationNode,Property.Publication.AFFILIATION, publication.getAffiliation());
			publicationNode.setProperty(Property.Publication.JOURNAL, publication.getJounral());
			publicationIndex.add(publicationNode,Property.Publication.JOURNAL, publication.getJounral());
			publicationNode.setProperty(Property.Publication.TITLE, publication.getTitle());
			publicationIndex.add(publicationNode,Property.Publication.TITLE, publication.getTitle());
			publicationNode.setProperty(Property.Publication.YEAR, publication.getYear());
			publicationIndex.add(publicationNode,Property.Publication.YEAR, publication.getYear());
			
			for (Iterator<PersonWrapper> iterator = publication.getAuthors().iterator(); iterator.hasNext();) {
				PersonWrapper author = (PersonWrapper) iterator.next();
				processPerson(author, modelNode, publicationNode, DocumentRelTypes.HAS_AUTHOR);			
			}
			publicationNode.addLabel(NodeLabel.Types.PUBLICATION);
			publicationNode.createRelationshipTo(referenceNode, DatabaseRelTypes.BELONGS_TO);
			referenceNode.createRelationshipTo(publicationNode, DocumentRelTypes.HAS_PUBLICATION);
		}
	}
	
	protected static void processPerson(PersonWrapper person, Node modelNode, Node referenceNode, RelationshipType relationToReference){
		Node personNode = null;
		person.repairNullStrings();
		
		if (!person.isValid()) return;
		try {
			PersonQuery pq = new PersonQuery();
			pq.addQueryClause(PersonFieldEnumerator.FAMILYNAME, person.getLastName());
			pq.addQueryClause(PersonFieldEnumerator.GIVENNAME, person.getFirstName());
			personNode = personIndex.query(pq.getQuery()).getSingle();
		} catch (NoSuchElementException  e) {
			personNode = null;
		}					
		//create a node for each creator & link persons between models
		if (personNode==null){
			personNode= graphDB.createNode();
			personNode.addLabel(NodeLabel.Types.PERSON);
			personNode.setProperty(Property.Person.GIVENNAME, person.getFirstName());
			personNode.setProperty(Property.Person.FAMILYNAME, person.getLastName());
			personNode.setProperty(Property.Person.ORGANIZATION, person.getOrganization());
			personNode.setProperty(Property.Person.EMAIL, person.getEmail());
			//add to person index
			personIndex.add(personNode, Property.Person.FAMILYNAME, person.getLastName());
			personIndex.add(personNode, Property.Person.GIVENNAME, person.getFirstName());
			personIndex.add(personNode, Property.Person.EMAIL, person.getEmail());
			personIndex.add(personNode, Property.Person.ORGANIZATION, person.getOrganization());

			//add to node index
			if (relationToReference.equals(DocumentRelTypes.IS_CREATOR)) {
				modelIndex.add(modelNode, Property.General.CREATOR, person.getFirstName());
				modelIndex.add(modelNode, Property.General.CREATOR, person.getLastName());
			} else {
				modelIndex.add(modelNode, Property.General.AUTHOR, person.getFirstName());
				modelIndex.add(modelNode, Property.General.AUTHOR, person.getLastName());
			}
		}

		//set relationships
		personNode.createRelationshipTo(referenceNode, Relation.DatabaseRelTypes.BELONGS_TO);
		referenceNode.createRelationshipTo(personNode, relationToReference);
	}
	


}
