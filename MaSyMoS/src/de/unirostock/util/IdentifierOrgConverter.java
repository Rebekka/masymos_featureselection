package de.unirostock.util;

import org.apache.commons.lang3.StringUtils;

import uk.ac.ebi.miriam.lib.MiriamLink;

public class IdentifierOrgConverter {
	
	public static String convertFromIdentifierOrgToMiriam(String uri){
		String oldUri = uri;
		try {
			MiriamLink link = new MiriamLink();
			link.setAddress("http://www.ebi.ac.uk/miriamws/main/MiriamWebServices");
			if (StringUtils.startsWith(uri, "http")) {
				uri = link.convertURL(uri);
			}
		}catch (Exception e) {
		
		}
		if (StringUtils.isEmpty(uri)) uri = oldUri;
		return uri;				
	}
	
	public static String convertFromMiriamToIdentifierOrg(String uri){
		String oldUri = uri;
		try {
			MiriamLink link = new MiriamLink();
			link.setAddress("http://www.ebi.ac.uk/miriamws/main/MiriamWebServices");
			if (StringUtils.startsWith(uri, "http")) {
				uri = link.convertURN(uri);
			}
		}catch (Exception e) {
		
		}
		if (StringUtils.isEmpty(uri)) uri = oldUri;
		return uri;				
	}

}
